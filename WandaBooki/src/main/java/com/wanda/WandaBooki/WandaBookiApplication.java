package com.wanda.WandaBooki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WandaBookiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WandaBookiApplication.class, args);
	}

}
